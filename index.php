<?php

error_reporting(E_ALL);

require 'controllers/controller.php';

$controller = new BaseController();

$app = new \Slim\Slim(array(
    'templates.path'=>'views'
));

$app->get('/', function(){
   echo 'mhhm..yess';
});

$app->group('/api/v1', function() use($app,$controller){

    $app->group('/oauth', function() use($app,$controller){
        $app->group('/google', function() use($app,$controller) {
            $app->get('/login', function () use ($app, $controller) {
                $app->render('oauth/google/login.php', array('constantsHandler'=>$controller->constantsHandler));
            });

            $app->get('/login/redirect', function () use ($app, $controller) {
                if(isset($_GET['code'])){
                    $code = $_GET['code'];
                    $controller->SocialController->GoogleOAuthSignin($code);

                    //ToDo: create auth token for login to validate API access
                }
            });
        });

        //ToDo: 0.1 - Facebook login
        $app->post('/facebook', function() use($app,$controller){});

    });

    //ToDo: 1.0 Accounts
    $app->group('/accounts', function() use($app,$controller) {
        //ToDo: 1.1 - Registration (Regular App User, Vendor User)
        $app->post('/registration/user', function() use($app,$controller){
            $controller->AccountsController->createAccount($_POST);
        });

        //ToDo: 1.2 - Login/Logout
        $app->post('/login', function() use($app,$controller){
            $controller->AccountsController->loginAccount($_POST);
        });

        $app->post('/logout', function() use($app,$controller){});

        //ToDo: 1.3 - Create vendor profile for account (create a vendor uuid as well)
        $app->put('/:accountuuid/profile', function($accountuuid) use($app,$controller){
            $controller->AccountsController->createVendorProfile($accountuuid);
        });
    });

    //ToDo: 2.0 Regular App Users
    $app->group('/user', function() use($app,$controller) {
        //ToDo: 2.1 - Edit Regular App User (personal details, contact information)
        $app->post('/:profileuuid/profile', function($profileuuid) use($app,$controller){
            $controller->AccountsController->updateAccountProfile($profileuuid,$_POST);
        });

        //ToDo: 2.2 - List following vendors

        //ToDo: 2.3 - Add/Remove Following vendors
    });

    //ToDo: 3.0 Vendor App Users
    $app->group('/vendor', function() use($app,$controller) {

        //ToDo: 3.1 - Edit Vendor App user (personal details, contact information, bio)
        $app->get('/:vendoruuid/profile', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.2 - Get Followers
        $app->get('/:vendoruuid/followers', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.3 - Get Vendor Availability
        $app->get('/:vendoruuid/availability', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.4 - Update Vendor Availability
        $app->post('/:vendoruuid/availability', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.5 - Get Vendor Packages (include price ranges)
        $app->get('/:vendoruuid/packages', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.6 - Update Vendor Packages (include price ranges)
        $app->post('/:vendoruuid/packages', function($vendoruuid) use($app,$controller){});

        //ToDo: 3.7 - Get Vendor ratings
        $app->get('/:vendoruuid/ratings', function($vendoruuid) use($app,$controller){});
    });

    //ToDo: 4.0 Vendors
    $app->group('/vendors', function() use($app,$controller){

        //ToDo: 4.1 - Search Vendors (with filter, category, price range, availability dates)
        $app->post('/search', function() use($app,$controller){});

        //ToDo: 4.1 - View Vendor
        $app->post('/view/:vendoruuid', function($vendoruuid) use($app,$controller){});

        //ToDo: 4.2 - Contact Vendor
        $app->post('/contact/:vendoruuid', function($vendoruuid) use($app,$controller){});

        //ToDo: 4.3 - Rate
        $app->post('/rate/:vendoruuid', function($vendoruuid) use($app,$controller){});

        //ToDo: 4.4 - Report Vendor
        $app->post('/report/:vendoruuid', function($vendoruuid) use($app,$controller){});

    });

});
$app->run();