<?php


require 'vendor/autoload.php';
require 'classes/DBHandler.php';
require 'classes/ConstantsHandler.php';
require 'classes/ErrorHandler.php';

require 'AccountsController.php';
require 'SocialController.php';

class BaseController {

    private $db;
    private $errorHandler;

    public $constantsHandler;
    public $AccountsController;
    public $SocialController;

    public function __construct()
    {
        $conn = new DatabaseConfig();

        $this->db = $conn->getConnection();

        $this->errorHandler = new ErrorHandler();
        $this->constantsHandler = new ConstantsHandler();

        $this->AccountsController = new AccountsController($this->constantsHandler, $this->errorHandler, $this->db);
        $this->SocialController = new SocialController($this->errorHandler, $this->constantsHandler, $this->AccountsController, $this->db);

    }
}