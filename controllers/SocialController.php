<?php

require 'vendor/google-api-php-client-2.4.1/vendor/autoload.php';


class SocialController
{

    private $db;

    private $errorHandler;
    private $constantsHandler;
    private $accountsController;

    public function __construct($err, $constants, $accounts, $db)
    {
        $this->db = $db;

        $this->errorHandler = $err;
        $this->constantsHandler = $constants;
        $this->accountsController = $accounts;
    }

    public function GoogleOAuthSignin($code){

        // init configuration
        $clientID = $this->constantsHandler->googleOAuthClientID;
        $clientSecret = $this->constantsHandler->googleOAuthClientSecret;
        $redirectUri = $this->constantsHandler->googleOAuthRedirectURI;

        // create Client Request to access Google API
        $client = new Google_Client();
        $client->setClientId($clientID);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($redirectUri);
        $client->addScope("email");
        $client->addScope("profile");

        $token = $client->fetchAccessTokenWithAuthCode($code);
        $client->setAccessToken($token['access_token']);

        // get profile info
        $google_oauth = new Google_Service_Oauth2($client);
        $google_account_info = $google_oauth->userinfo->get();

        $email = $google_account_info->email;
        $id = $google_account_info->id;

        //create account if not exists, if exists update social value
        $this->accountsController->createAccount(array('username'=>$email,'password'=>$id,'social_type'=>0), true);

        var_dump($email,'success');
    }

}