<?php

class AccountsController{

    private $db;
    private $constantsHandler;
    private $errorHandler;

    public function __construct($constantsHandler, $errorHandler, $db)
    {
        $this->constantsHandler = $constantsHandler;
        $this->errorHandler = $errorHandler;
        $this->db = $db;
    }

    public function loginAccount($data){

        $user = $this->getAccountByUsername($data['username']);

        if(!empty($user)){
            $passwordVerify = password_verify($data['password'], $user['password']);

            if($passwordVerify){
                unset($user['password']);
                return $user;
            }
            return null;
        }
        return null;
    }

    ##START Account Creation##

    /**
     * Find Account by Username
     * @param $username
     * @return mixed
     */
    public function getAccountByUsername($username){
        $getAccount = $this->db->prepare("SELECT * FROM accounts WHERE username = :username");
        $getAccount->bindParam('username',$username);
        $getAccount->execute();

        $account = $getAccount->fetch(PDO::FETCH_ASSOC);

        return $account;
    }

    /**
     * Find Account by UUID
     * @param $uuid
     * @return mixed
     */
    public function getAccountByUUID($uuid){
        $getAccount = $this->db->prepare("SELECT * FROM accounts WHERE uuid = :uuid");
        $getAccount->bindParam('uuid',$uuid);
        $getAccount->execute();

        $account = $getAccount->fetch(PDO::FETCH_ASSOC);

        return $account;
    }

    /**
     * Check Account has Vendor Profile
     * @param $uuid
     * @return mixed
     */
    public function checkAccountUserProfileExists($uuid){
        $getAccount = $this->db->prepare("SELECT * FROM accounts_profiles WHERE fk_accounts_uuid = :uuid");
        $getAccount->bindParam('uuid',$uuid);
        $getAccount->execute();

        $account = $getAccount->fetch(PDO::FETCH_ASSOC);

        return $account;
    }

    /**
     * Check Account has Vendor Profile
     * @param $uuid
     * @return mixed
     */
    public function checkAccountVendorProfileExists($uuid){
        $getAccount = $this->db->prepare("SELECT * FROM vendor_profiles WHERE fk_accounts_uuid = :uuid");
        $getAccount->bindParam('uuid',$uuid);
        $getAccount->execute();

        $account = $getAccount->fetch(PDO::FETCH_ASSOC);

        return $account;
    }

    /**
     * Create Account - User
     * @param $data (username, password)
     * @param bool $social
     * @return bool
     */
    public function createAccount($data,$social = false){
        $username = htmlentities($data['username']);
        $password = htmlentities($data['password']);

        $account = $this->getAccountByUsername($username);

        $accountuuid = $this->constantsHandler->v4UUID();

        if(!empty($account)) {
            if(!$social){
                return $this->errorHandler->throwError(001, 01);
            }else{
                $accountuuid = $account["uuid"];
                return $this->updateAccountSocialID($accountuuid, $data['password'],$data['social_type']);
            }
        }


        $password = password_hash($password, PASSWORD_BCRYPT);

        $createAccount = $this->db->prepare("INSERT INTO accounts(uuid,username,password,fk_accounts_type_id, DateCreated, LastLogin) 
                                             VALUES(:uuid,:username, :password, 1, CURRENT_TIMESTAMP(), NULL)");
        $createAccount->bindParam('uuid', $accountuuid);
        $createAccount->bindParam('username', $username);
        $createAccount->bindParam('password', $password);
        $createAccount->execute();

        $this->createAccountProfile($accountuuid);

        die(json_encode(array('error'=>0,'body'=>$accountuuid)));
    }

    /**
     * Create User Profile
     */
    public function createAccountProfile($accountuuid){

        if(empty($accountuuid)){
            return $this->errorHandler->throwError(001,04);
        }

        $profileuuid = $this->constantsHandler->v4UUID();

        $account = $this->getAccountByUUID($accountuuid);
        $userProfile = $this->checkAccountVendorProfileExists($accountuuid);

        if(empty($account)){
            return $this->errorHandler->throwError(001,05);
        }

        if(!empty($userProfile)){
            return $this->errorHandler->throwError(001,06);
        }

        $createUProfile = $this->db->prepare("INSERT INTO accounts_profiles(uuid, fk_accounts_uuid) VALUES(:uuid,:accountuuid)");
        $createUProfile->bindParam('uuid', $profileuuid);
        $createUProfile->bindParam('accountuuid', $accountuuid);
        $createUProfile->execute();

        die(json_encode(array('error'=>0,'body'=>$profileuuid)));
    }

    /**
     * Create Vendor Profile
     * @description Creates vendor profile for an account allowing them to create listings
     * @param $accountuuid
     * @return bool
     */
    public function createVendorProfile($accountuuid){

        if(empty($accountuuid)){
            return $this->errorHandler->throwError(001,04);
        }

        $vendoruuid = $this->constantsHandler->v4UUID();

        $account = $this->getAccountByUUID($accountuuid);
        $vendorProfile = $this->checkAccountVendorProfileExists($accountuuid);

        if(empty($account)){
            return $this->errorHandler->throwError(001,05);
        }

        if(!empty($vendorProfile)){
            return $this->errorHandler->throwError(001,06);
        }

        $createVendorProfile = $this->db->prepare("INSERT INTO vendor_profiles(uuid, fk_accounts_uuid) VALUES(:uuid,:accountuuid)");
        $createVendorProfile->bindParam('uuid', $vendoruuid);
        $createVendorProfile->bindParam('accountuuid', $accountuuid);
        $createVendorProfile->execute();

        die(json_encode(array('error'=>0,'body'=>$vendoruuid)));
    }

    public function updateAccountSocialID($accountuuid, $socialid, $socialtype){

        $socialtypecol = '';

        switch($socialtype){
            case 0:
                $socialtypecol = 'google_id';
                break;
            case 1:
                $socialtypecol = 'facebook_id';
                break;
        }

        $account = $this->getAccountByUUID($accountuuid);

        if(empty($account)){
            return $this->errorHandler->throwError(001,05);
        }

        $updateAS = $this->db->prepare("UPDATE accounts SET $socialtypecol = :idval WHERE uuid = :uuid");
        $updateAS->bindParam('idval', $socialid);
        $updateAS->bindParam('uuid', $accountuuid);
        $updateAS->execute();

        die(json_encode(array('error'=>0,'body'=>$accountuuid)));
    }

    ##END Account Creation##

    ##START Profile Modification

    public function updateAccountProfile($profileuuid, $data){

        if(empty($profileuuid)){
            $this->errorHandler->throwError(001,04);
        }

        $profile = $this->checkAccountUserProfileExists($profileuuid);
        if(!empty($profile)){
            $this->errorHandler->throwError(001,07);
        }

        $firstname = $data['firstname'];
        $lastname = $data['lastname'];

        $updateAP = $this->db->prepare("UPDATE accounts_profiles 
                                        SET 
                                          firstname = :firstname,
                                          lastname = :lastname
                                        WHERE
                                          uuid = :uuid
        ");

        $updateAP->bindParam('uuid', $profileuuid);
        $updateAP->bindParam('firstname', $firstname);
        $updateAP->bindParam('lastname', $lastname);
        $updateAP->execute();

        die(json_encode(array('error'=>0,'body'=>$profileuuid)));
    }

    ##END Profile Modification

    /**
     * Send confirmation email
     * @description Used for registration of all accounts.
     * @description Includes call to secondary function to send confirmation email to convert user to vendor account
     */
    public function sendConfirmationEmail($accountId){
        //ToDo: sendConfirmationEmail()
    }

}