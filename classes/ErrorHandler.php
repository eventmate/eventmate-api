<?php

class ErrorHandler
{

    private $ErrorList  = array(
        'codes'=>
            array(
            001=>//Accounts
                array(
                    01=>array(403,'Username Already in Use'),
                    03=>array(403,'Invalid Login Details'),
                    04=>array(403,'Missing required fields'),
                    05=>array(403,'Account Does Not Exist'),
                    06=>array(403,'Profile Already Exists'),
                    07=>array(403,'Profile Does Not Exist'),
                )
            )
    );

    public function throwError($code,$subcode){
        $error = $this->ErrorList['codes'][$code][$subcode];

        $headerCode = $error[0];
        $headerMessage = $error[1];

        $body = array(
            'error'=>$code.$subcode.$headerCode,
            'body'=>$headerMessage
        );

        ob_end_clean();
        header("HTTP/1.1 $headerCode $headerMessage");
        echo json_encode($body);
    }
}