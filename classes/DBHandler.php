<?php

class DatabaseConfig {

    protected $db;

    public function DatabaseConfig(){

        $conn = NULL;

        $host = 'localhost';
        $dbname = 'eventmate';
        $user = 'root';
        $pass = 'x7rDJVTa';

        $dsn = "mysql:host=".$host.";dbname=".$dbname;

        try{
            $conn = new PDO($dsn, $user, $pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e){
            echo 'ERROR: ' . $e->getMessage();
        }
        $this->db = $conn;
    }

    public function getConnection(){
        return $this->db;
    }
}